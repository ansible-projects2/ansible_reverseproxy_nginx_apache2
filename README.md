# Ansible - Nginx as a Web Server and Reverse Proxy for Apache



## Yaml file

```
---
- name: "Nginx as a Web Server and Reverse Proxy for Apache "
  hosts: all
  become: true
  vars_files:
    - variables.yaml

  tasks:


    - name: "repositry enabling"
      command: "add-apt-repository ppa:ondrej/php -y"


    - name: "service"
      command: "apt-get update"


    - name: "apache,php7 installation"
      apt:
        name:
          - apache2
          - mysql-server
          - php7.4
          - nginx

        state: present

    - name: "copying apache  custom port"
      template:
        src: ports.conf.tmpl
        dest: "/etc/apache2/ports.conf"

    - name: "copying virtual host file"
      template:
        src: virtualhost.conf.template
        dest: "/etc/apache2/sites-available/{{ sitename }}.conf"

    - name: "default siet disabling in apache"
      command: a2dissite 000-default.conf

    - name: "site enabling in apache"
      command: a2ensite {{ sitename }}.conf

    - name: "document root directory creation"
      file:
        path: "/var/www/html/{{ sitename }}"
        state: directory

    - name: "index file copying"
      template:
        src:  index.tmpl
        dest: "/var/www/html/{{ sitename }}/index.html"
    
    - name: "copying nginx config for the domain"
      template:
        src: domainconfig.tmpl
        dest: "/etc/nginx/sites-available/{{ sitename }}.conf"
    
    - name: "symlink for virtual host"
      file: 
        src: "/etc/nginx/sites-available/{{ sitename }}.conf"
        dest: "/etc/nginx/sites-enabled/{{ sitename }}.conf" 
        state: link
  

    - name: "Services"
      service:
        name: "{{ item }}"
        state: restarted
        enabled: true
      with_items:
        - apache2
        - mysql
        - nginx


```


